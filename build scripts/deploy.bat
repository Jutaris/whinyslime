butler push ..\builds\whineslime.zip --if-changed BrightMothGames/Whiny-Slime:html
IF ERRORLEVEL 1 GOTO :error 
butler push ..\builds\whineslime-linux.zip --if-changed BrightMothGames/Whiny-Slime:linux
IF ERRORLEVEL 1 GOTO :error
butler push ..\builds\whineslime-win.zip --if-changed BrightMothGames/Whiny-Slime:win
IF ERRORLEVEL 1 GOTO :error
butler push ..\builds\whineslime-raspi.zip --if-changed BrightMothGames/Whiny-Slime:raspi
IF ERRORLEVEL 1 GOTO :error
butler push ..\builds\whineslime-mac.zip BrightMothGames/Whiny-Slime:mac
IF ERRORLEVEL 1 GOTO :error
butler push ..\whinyslime.p8.png BrightMothGames/Whiny-Slime:cart
GOTO :done

:error
EXIT /B 1

:done
PAUSE
PUSHD ..
pico8 "./ld46.p8" -export "whineslime.js"
pico8 "./ld46.p8" -export "whineslime.bin"
POPD

7z a ..\builds\whineslime.zip @web-build.txt
IF ERRORLEVEL 1 GOTO :error
7z a ..\builds\whineslime-win.zip @win-build.txt
IF ERRORLEVEL 1 GOTO :error
7z a ..\builds\whineslime-mac.zip @mac-build.txt
IF ERRORLEVEL 1 GOTO :error
7z a ..\builds\whineslime-linux.zip @linux-build.txt
IF ERRORLEVEL 1 GOTO :error
7z a ..\builds\whineslime-raspi.zip @raspi-build.txt
IF ERRORLEVEL 1 GOTO :error
GOTO :done

:error
EXIT /B 1

:done
